# Lean Canvas Modeler - LCM

## Story

The LCM is a tool born out of frustration. For years, I found that there were either no tools to easily create Lean and Business Model canvases, or they were hidden behind a paywall in other platforms. The basic functionality of such a tool is fairly simple: an editor that lets you mock and edit as many canvases as you want. So, I decided to create one myself.

## Features

The LCM is a web-based tool that allows you to create, edit, and export Lean and Business Model canvases. Here are some of its key features:

1. **Multiple Canvas Editing:** You can create and edit as many canvases as you want. Each canvas is divided into sections according to the Lean Canvas and Business Model Canvas templates.

2. **Markdown Support:** The editor supports Markdown, a lightweight markup language with plain-text-formatting syntax. This allows you to format your text in a way that's easy to write and read.

3. **Live Preview:** As you type in the editor, the preview pane updates in real time to show your formatted text. This allows you to see exactly what your canvas will look like as you're creating it.

4. **Tooltips:** Each section of the canvas has a tooltip that provides a brief explanation of what the section is for. This can be especially helpful if you're new to Lean or Business Model canvases.

5. **Export Functionality:** You can export your canvas as an image, as a JSON file or as a Markdown file. This allows you to easily share your canvas with others or save it for later use.

6. **Import Functionality:** You can also import a canvas from a JSON file. This allows you to continue working on a canvas that you've previously saved.

## Usage

To use the LCM, simply open the `index.html` file in your web browser if you have cloned or downloaded the reposity, or navigate to <https://lcm.niftylittletools.dev/> to use the tool online.

You'll see a blank canvas that you can start editing right away. To edit a section of the canvas, click on the corresponding editor and start typing. To export your canvas, click on the "Export as" button and choose either "Image" or "JSON". To import a canvas, click on the "Import JSON" button and select the JSON file you want to import.
